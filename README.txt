This is a readme for Singapore_Sectoral_Reference_Emissions_Simulation

############ License #############
The MIT License (MIT)

Copyright (c) 2021 Aniq Ahsan

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


########## How to Use ###########
Packages required
The following packages are required for the code to work is given in DEPENDENCIES.txt


To run the code, you will need to run sectoral_reference_simulation.py using your preferred IDE. The code will plot a figure as an output. You will need to  provide 2 .csv files; these are described in the "Files" section.
You will also need to set a few constants or simulation parameters; these are described in the "Constants" section.
The code has a few printing options; these are described in the "Printing" section.

Files (Data input)
The code takes two .csv files as inputs.
emissions_data_filename : this is the name (+ location) of the .csv file that contains the historical annual energy balance table values for various fuel types. The IEA energy balance table format and definitions are used. Please look at IEA_data_total.csv for an example of the format of the .csv file. These could be estimates from other sources, but need to follow the IEA energy balance table format.
emissions_factors_filename : this is the name (+ location) of the .csv file that contains the lower and upper values of conversion factor and carbon emission factor of various fuels. It also contains the sigma, mu, mean, variance, mode and median for these values fitted to a lognormal distribution. Emissions_Factors.csv contains the values from 2006 IPCC Guidelines. Only update this if you are are using updated values or are testing other values.

results_filename : this is the name (+ location) of the .csv that the results will be saved in.


Constants (Simulation parameters)
Q_ALPHA_1 and Q_ALPHA_2 : These are the normal ppf values of the lower and upper values. These are set to 5% and 95% cumulative values as defined by IPCC. Only change these is you have a different lower and upper values of the factors.
Q_FACTOR : this is an intermediate value used for simulation
BASE_ERROR : This is the base error of each input value. This is set to 2%, but it may be changed if you wish to test situations where the base error of input values of data in emissions_data_filename are higher or lower.


N : This is the number of monte carlo runs. The paper used N =1000. But the exmaple is set to 100; this takes about a min to run on a macbook air 2016.
M : This is the number of runs to print. This is useful for very high N as the plot may be too difficult to be seen.



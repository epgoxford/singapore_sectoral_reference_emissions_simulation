#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 23 13:56:41 2020

@author: aniqahsan
"""

######### Packages ############
#import csv
import scipy as sp
import pandas as pd
import numpy as np
import numpy.random
import matplotlib.pyplot as plt
import time
import scipy.stats



##### files  #########
data_folder = r"/Users/aniqahsan/Documents/Oxford_PhD/Singapore Sectoral and Reference Emissions Simulation/data/"
emissions_data_filename = data_folder + r"IEA_data_total.csv"
emissions_factors_filename = data_folder + r"Emissions_Factors.csv"

results_folder = r"/Users/aniqahsan/Documents/Oxford_PhD/Singapore Sectoral and Reference Emissions Simulation/results/"
results_filename = results_folder + r"test_output_file.csv"

######### Constants ############

Q_ALPHA_1 = sp.stats.norm.ppf(0.05)
Q_ALPHA_2 = sp.stats.norm.ppf(0.95)
Q_FACTOR = 1/(Q_ALPHA_2-Q_ALPHA_1)
BASE_ERROR = 1E-2

N = 100 #Paper has N = 1000. It is currently set to 100 as an example. This takses about a min on a macbook air 2016
M = N #values to print

sectoral_types = np.array([
    "electricity plants",
    "energy industry own use",
    "industry",
    "transport",
    "residential",
    "commercial",
    "non-specified",
    ])

reference_types = np.array([
    "imports",                    
    "exports",
    "international marine bunker",
    "international aviation bunker",
    "stock changes",
    "non-energy use",
    ])

refinery_types = np.array([
    "oil refineries",
    "production",
    ])

all_types = np.array([
    ["electricity plants",1],
    ["energy industry own use",1],
    ["industry",1],
    ["transport",1],
    ["residential",1],
    ["commercial",1],
    ["non-specified",1],
    ["imports",1],                        
    ["exports",1],
    ["international marine bunker",1],
    ["international aviation bunker",1],
    ["stock changes",1],
    ["non-energy use",1],
    ["oil refineries",1],
    ["production",1],
    ])
all_types_df = pd.DataFrame(data = all_types, columns = ["type name","switch sign"]) #sign to change the sign to appropiate value so that it can be added

######## Functions ########

def single_emission_fun(inputs):
    alpha, mu_1, sigma_1, mu_2, sigma_2, switch_sign = inputs
    alpha = 1E-300 if (alpha == 0) else alpha
    sign = 1.0 if (alpha > 0) else -1.0
    sigma2_3 = np.log(1 + BASE_ERROR**2) #assuming std is BASE_ERROR*alpha
    mu_3 = np.log(sign*alpha) - 0.5*sigma2_3
    mu = mu_1 + mu_2 + mu_3 + np.log(44.0/12.0/1000.0)
    sigma2 = sigma_1**2 + sigma_2**2 + sigma2_3
    return switch_sign * sign * np.exp(mu + (sigma2**0.5) * np.random.randn())


def single_emission_fun(inputs):
    alpha, lower_1, upper_1, lower_2, upper_2 = inputs
    alpha = 1E-300 if (alpha == 0) else alpha
    sign = 1.0 if (alpha > 0) else -1.0
    lnqx_lower_1 = np.log(lower_1)
    lnqx_upper_1 = np.log(upper_1)
    lnqx_lower_2 = np.log(lower_2)
    lnqx_upper_2 = np.log(upper_2)
    sigma_1 = Q_FACTOR * (lnqx_upper_1 - lnqx_lower_1)
    sigma_2 = Q_FACTOR * (lnqx_upper_2 - lnqx_lower_2)
    mu_1 = lnqx_upper_1 - sigma_1*Q_ALPHA_2
    mu_2 = lnqx_upper_2 - sigma_2*Q_ALPHA_2
    sigma2_3 = np.log(1 + BASE_ERROR**2) #assuming std is BASE_ERROR*alpha
    mu_3 = np.log(sign*alpha) - 0.5*sigma2_3
    mu = mu_1 + mu_2 + mu_3 + np.log(44.0/12.0/1000.0)
    sigma2 = sigma_1**2 + sigma_2**2 + sigma2_3
    return sign * np.exp(mu + (sigma2**0.5) * np.random.randn())

emission_fun_single_year = np.vectorize(single_emission_fun, signature = '(n)->()')

emission_fun_multiple_year = np.vectorize(emission_fun_single_year, signature = '(n)->()')

def filter_data_type_fun(data_df, types_array):
    total_con = 0
    
    for counter1 in range(np.shape(types_array)[0]):
        tcon = data_df["type name"] == types_array[counter1]
        total_con = total_con | tcon
    
    filtered_df = data_df[total_con].reset_index(drop = True)
    return filtered_df


def yearly_df_generator_fun(input_df):
    years_array = np.sort(np.unique(combined_emissions_data_df["year"]))
    output_array = np.zeros((len(years_array),int(input_df.shape[0]/len(years_array)),6))
    for counter1 in range(len(years_array)):
        tdf = input_df[input_df["year"] == years_array[counter1]]
        output_array[counter1,:,0] = np.array(tdf["value"],dtype = np.double)
        output_array[counter1,:,1] = np.array(tdf["mu 1"],dtype = np.double)
        output_array[counter1,:,2] = np.array(tdf["sigma 1"],dtype = np.double)
        output_array[counter1,:,3] = np.array(tdf["mu 2"],dtype = np.double)
        output_array[counter1,:,4] = np.array(tdf["sigma 2"],dtype = np.double)
        output_array[counter1,:,5] = np.array(tdf["switch sign"],dtype = np.double)
    return output_array

def yearly_df_generator_fun(input_df):
    years_array = np.sort(np.unique(combined_emissions_data_df["year"]))
    output_array = np.zeros((len(years_array),int(input_df.shape[0]/len(years_array)),5))
    for counter1 in range(len(years_array)):
        tdf = input_df[input_df["year"] == years_array[counter1]]
        output_array[counter1,:,0] = np.array(tdf["value"],dtype = np.double)
        output_array[counter1,:,1] = np.array(tdf["lower 1"],dtype = np.double)
        output_array[counter1,:,2] = np.array(tdf["upper 1"],dtype = np.double)
        output_array[counter1,:,3] = np.array(tdf["lower 2"],dtype = np.double)
        output_array[counter1,:,4] = np.array(tdf["upper 2"],dtype = np.double)
    return output_array


def simulate_everything_once(sectoral_yearly_inputs_array,reference_yearly_inputs_array,refinery_yearly_inputs_array):
    sectoral_emissions = emission_fun_multiple_year(sectoral_yearly_inputs_array).sum(axis = 1)
    reference_emissions = emission_fun_multiple_year(reference_yearly_inputs_array).sum(axis = 1)
    refinery_emissions = emission_fun_multiple_year(refinery_yearly_inputs_array).sum(axis = 1)
    return np.array([sectoral_emissions,reference_emissions,refinery_emissions])

def simulate_everything_multiple_times(sectoral_yearly_inputs_array,reference_yearly_inputs_array,refinery_yearly_inputs_array, N):
    total_results = []
    for counter1 in range(N):
        total_results += [simulate_everything_once(sectoral_yearly_inputs_array,reference_yearly_inputs_array,refinery_yearly_inputs_array)]
    return np.array(total_results)

####### Load Data #######
emissions_factors_df = pd.read_csv(emissions_factors_filename)
emissions_data_df = pd.read_csv(emissions_data_filename)



###### Combine usage data and factors ############
combined_emissions_data_df = pd.merge(emissions_data_df,emissions_factors_df, on="fuel name")
combined_emissions_data_df = pd.merge(combined_emissions_data_df, all_types_df, on = "type name")

###### Fiter out data #######

### Fitler Sectoral Data #####

sectoral_emissions_data_df = filter_data_type_fun(combined_emissions_data_df,sectoral_types)
reference_emissions_data_df = filter_data_type_fun(combined_emissions_data_df,reference_types)
refinery_emissions_data_df = filter_data_type_fun(combined_emissions_data_df,refinery_types)


########## generate location for each year #######
sectoral_yearly_inputs_array = yearly_df_generator_fun(sectoral_emissions_data_df)
reference_yearly_inputs_array = yearly_df_generator_fun(reference_emissions_data_df)
refinery_yearly_inputs_array = yearly_df_generator_fun(refinery_emissions_data_df)


########## run simulation ######

start_time = time.time()
multiple_results = simulate_everything_multiple_times(sectoral_yearly_inputs_array,
                        reference_yearly_inputs_array,refinery_yearly_inputs_array, N)
end_time = time.time()
time_taken = end_time - start_time

print("Time taken to do ", N, "runs = ", time_taken, "s")




# ####### Save data #######
# # results_filename = results_folder + r"flattened_multiple_results_3.csv"
# flattened_multiple_results = multiple_results.flatten()
# results_df = pd.DataFrame(data = flattened_multiple_results, columns = ["results"])
# results_df.to_csv(results_filename, index = False)


# ###### Load data ########
# loaded_df = pd.read_csv(results_filename)
# reshaped_multiple_results_3 = loaded_df.values.reshape((10000,3,24)) #script to reshape the loaded result


# ##### Combine multiple files ######
# combined_reshaped_multiple_results = np.append(reshaped_multiple_results_1,reshaped_multiple_results_2, axis = 0)
# combined_reshaped_multiple_results= np.append(combined_reshaped_multiple_results, reshaped_multiple_results_3, axis = 0)

combined_reshaped_multiple_results = multiple_results



#### Print results #######

O = np.shape(combined_reshaped_multiple_results)[0]
years_array = np.sort(np.unique(combined_emissions_data_df["year"]))
### Choose indices to print ####
indices = np.random.randint(0,O,M)
results_to_print = combined_reshaped_multiple_results[indices]

lower_limit_array = np.zeros((4,np.size(years_array)))
upper_limit_array = np.zeros((4,np.size(years_array)))
lower_location = np.int(O*0.025)
upper_location = np.int(O*0.925)

for counter1 in range(3):
    for counter2 in range(np.size(years_array)):
        test_array = np.sort(combined_reshaped_multiple_results[:,counter1,counter2])
        lower_limit_array[counter1, counter2] = test_array[lower_location]
        upper_limit_array[counter1, counter2] = test_array[upper_location]

for counter1 in range(np.size(years_array)):
    test_array = np.sort(combined_reshaped_multiple_results[:,1,counter1]-combined_reshaped_multiple_results[:,2,counter1])
    lower_limit_array[3, counter1] = test_array[lower_location]
    upper_limit_array[3, counter1] = test_array[upper_location]

alpha_value = 0.01
fig, ax = plt.subplots()
ax.plot([],[],"g", label = "Sectoral Approach")
ax.plot([],[],"r", label = "Reference Approach")
for counter1 in range(M):
    ax.plot(years_array, results_to_print[counter1,0]/1000, "g", alpha = alpha_value)
    ax.plot(years_array, (results_to_print[counter1,1] - results_to_print[counter1,2])/1000, "r", alpha = alpha_value)

ax.plot(years_array, lower_limit_array[0]/1000, "g")
ax.plot(years_array, upper_limit_array[0]/1000, "g")
ax.plot(years_array, lower_limit_array[3]/1000, "r")
ax.plot(years_array, upper_limit_array[3]/1000, "r")
        
ax.set(
    ylabel="Energy Related Annual \n GHG Emissions (MT CO$_2$)", 
    xlabel = "Year",
    title = "Simulation of Singapore's Annual Emissions \n Using Sectoral and Reference Approaches",
    )
ax.grid()
ax.legend()
plt.show()
